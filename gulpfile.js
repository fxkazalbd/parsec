'use strict';
 
const gulp = require('gulp');
const babel = require("gulp-babel");
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const pump = require('pump');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const newer = require('gulp-newer');
const flatten = require('gulp-flatten');
const del = require('del');
const browserSync = require('browser-sync').create();
sass.compiler = require('node-sass');
const cached = require('gulp-cached');
const remember = require('gulp-remember');
const path = require('path');
const pug = require('gulp-pug');
const pugInheritance = require('gulp-pug-inheritance');
const beautify = require('gulp-beautify');
const zip = require('gulp-zip');
const notify = require("gulp-notify");
const jshint = require('gulp-jshint');
const stylish = require('jshint-stylish');
const plumber = require('gulp-plumber');
const debug = require('gulp-debug');


// json animation files
gulp.task('jsons', function() {
    return gulp.src('./src/jsons/**/*.*')
        // .pipe(flatten())
        .pipe(newer('./dist/jsons'))
        // .pipe(debug({title: 'f-> '}))
        .pipe(gulp.dest('./dist/jsons'));
});

// views
gulp.task('views', function() {
    return gulp.src('./src/views/*.pug')
        .pipe(plumber({
            errorHandler: notify.onError({
                title: "Pug Error!",
                message: "Error: <%= error.message %>"
            })
        }))
        // .pipe(debug({title: 'File: '}))
        .pipe(pugInheritance({
            basedir: './src/views',
            skip: 'node_modules'
        }))
        .pipe(pug())
        .pipe(beautify.html({
            indent_size: 4,
            inline: []
        }))
        .pipe(gulp.dest('./dist'))
        .pipe(browserSync.stream());
});

// fonts
gulp.task('fonts', function() {
    return gulp.src('./src/fonts/**/*.*')
        .pipe(flatten())
        .pipe(newer('./dist/fonts'))
        // .pipe(debug({title: 'f-> '}))
        .pipe(gulp.dest('./dist/fonts'));
});

// images
gulp.task('images', function() {
    return gulp.src('./src/images/**/*.*')
        .pipe(flatten())
        .pipe(newer('./dist/images'))
        // .pipe(debug({title: 'img-> '}))
        // .pipe(imagemin([
        //     imagemin.gifsicle({
        //         interlaced: true,
        //         optimizationLevel: 3
        //     }),
        //     imagemin.jpegtran({
        //         progressive: true
        //     }),
        //     imagemin.optipng({
        //         optimizationLevel: 5
        //     }),
        //     imagemin.svgo({
        //         plugins: [
        //             {removeViewBox: true},
        //             {cleanupIDs: false}
        //         ]
        //     })
        // ], {
        //     // enabling this will log info on every image passed to gulp-imagemin
        //     verbose: false
        // }))
        .pipe(gulp.dest('./dist/images'));
});

// styles
gulp.task('styles', function () {
    return gulp.src([
        './src/styles/plugins/*.css',
        './src/styles/main.scss',
    ], { since: gulp.lastRun('styles') })
        .pipe(plumber({
            errorHandler: notify.onError({
                title: "SASS Error!",
                message: "Error: <%= error.message %>"
            })
        }))
        // .pipe(debug({title: 'File:'}))
        .pipe(sourcemaps.init({
            largeFile: true
        }))
        .pipe(cached('styles'))
        .pipe(remember('styles'))
        .pipe(sass({
            indentType: 'tab',
            indentWidth: 1,
            outputStyle: 'expanded',
        }))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 15 versions'],
            grid: true,
        }))
        .pipe(concat("main.min.css"))
        .pipe(cleanCSS({
            // format: 'beautify',
            level: 1, // optimization level: 0, 1(default), 2
            compatibility: 'ie7'
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/styles/'))
        .pipe(browserSync.stream());
});

// scripts
gulp.task('scripts', function (cb) {
    pump([
        gulp.src([
            './src/scripts/jquery/jquery.min.js',
            './src/scripts/plugins/*.js',
            './src/scripts/modules/*.js',
            './src/scripts/scripts.js',
        ], { since: gulp.lastRun('scripts') }),
        
        plumber(),
        jshint({
            esversion: 9 // ES9
        }),
        jshint.reporter(stylish),
        // use gulp-notify as jshint reporter
        notify(function (file) {
            if (file.jshint.success) { return false; }
            return file.relative + " (" + file.jshint.results.length + " errors!)";
        }),
        // debug({ title: 'File:' }),
        sourcemaps.init({
            largeFile: true
        }),
        cached('scripts'),
        remember('scripts'),
        babel({
            "presets": ["@babel/preset-env"]
        }),
        concat("main.min.js"),
        uglify({
            ie8: true,
            // warnings: false
        }),
        sourcemaps.write('./'),
        gulp.dest('./dist/scripts/'),
        browserSync.stream()
    ], cb)
});

// serve - browserSync
gulp.task('serve', function () {
    browserSync.init({
        server: {
            baseDir: "./dist",
            directory: true
        }
    });
});

// generate zip file - dist folder
gulp.task('zip-dist', function() {
    return gulp.src('./dist/**')
        .pipe(zip('zip-dist.zip'))
        .pipe(gulp.dest('./zip'));
});

// generate zip file - full project
gulp.task('zip-full', function() {
    return gulp.src(['./**', '!node_modules/**'])
        .pipe(zip('zip-full.zip'))
        .pipe(gulp.dest('./zip'));
});

// delete dist folder
gulp.task('clean', function() {
    return del('./dist').then(paths => {
        console.log('Deleted files & folders:', paths.join(''));
    });
});

// watcher
gulp.task('watcher', function() {
    gulp.watch('./src/views/**/*.*', gulp.series('views'));
    gulp.watch('./src/fonts/**/*.*', gulp.series('fonts'));
    gulp.watch('./src/jsons/**/*.*', gulp.series('jsons')); // only for json animation
    gulp.watch('./src/images/**/*.*', gulp.series('images'));

    gulp.watch('./src/styles/**/*.*', gulp.series('styles')).on('unlink', function(filepath) {
        remember.forget('styles', path.resolve(filepath));
        delete cached.caches.styles[path.resolve(filepath)];
    });

    gulp.watch('./src/scripts/**/*.*', gulp.series('scripts')).on('unlink', function(filepath) {
        remember.forget('scripts', path.resolve(filepath));
        delete cached.caches.scripts[path.resolve(filepath)];
    });
});

// default - build dist folder
gulp.task('default', gulp.series(
    gulp.series('clean'),
    gulp.parallel('views', 'fonts', 'jsons', 'images', 'styles', 'scripts')
));

// default and watch
gulp.task('watch', gulp.series(
    gulp.series('clean'),
    gulp.parallel('views', 'fonts', 'jsons', 'images', 'styles', 'scripts'),
    gulp.series('watcher')
));

// default, watch and browserSync
gulp.task('watch:live', gulp.series(
    gulp.series('clean'),
    gulp.parallel('views', 'fonts', 'jsons', 'images', 'styles', 'scripts'),
    gulp.parallel('watcher', 'serve')
));