// // API KEY: AIzaSyAd1_9wafTrKjPVPCH6t6MlhXrBJnDeFyQ

// const mapDiv = document.getElementById('map');

// const mapOptions = {
//     center: { lat: 40.854700, lng: -73.877500 },
//     zoom: 4,
//     disableDefaultUI: true,
// };

// const places = [
//     {
//         lat: 40.854700,
//         lng: -73.877500,
//         type: 'bronze',
//         title: 'ACS AG',
//         subtitle: 'Bronze Partner',
//         name: 'Roland Bieri',
//         position: 'CEO',
//         phone: '+41 (71) 929-4410',
//         email: 'bieri@acs-ag.ch',
//         website: 'http://example.com',
//         region: 'Europe',
//     },
//     {
//         lat:40.039900,
//         lng:-82.873230,
//         type: 'bronze',
//         title: 'ACS AG',
//         subtitle: 'Bronze Partner',
//         name: 'Roland Bieri',
//         position: 'CEO',
//         phone: '+41 (71) 929-4410',
//         email: 'bieri@acs-ag.ch',
//         website: 'http://example.com',
//         region: 'Europe',
//     },
//     { 
//         lat:41.621730,
//         lng:-73.774320,
//         type: 'gold',
//         title: 'Actemium Netherlands',
//         subtitle: 'Gold Partner',
//         name: 'Peter De Wit',
//         position: 'Business Development Director',
//         phone: '+31 (0)6 51 92 68 72',
//         email: 'peter.dwit@actemium.com',
//         website: 'http://example.com',
//         region: 'Worldwide',
//     },
//     {
//         lat:43.025030,
//         lng:-78.857250,
//         type: 'silver',
//         title: 'ASM Soft S.L.',
//         subtitle: 'Silver Partner',
//         name: 'Cesareo Barciela Carballido',
//         position: 'Managing Director',
//         phone: '+34 986 22 68 00',
//         email: 'cbarciela@asm.es',
//         website: 'http://example.com',
//         region: 'Europe',
//     },
//     {
//         lat:40.686620,
//         lng:-73.972830,
//         type: 'silver',
//         title: 'ASM Soft S.L.',
//         subtitle: 'Silver Partner',
//         name: 'Cesareo Barciela Carballido',
//         position: 'Managing Director',
//         phone: '+34 986 22 68 00',
//         email: 'cbarciela@asm.es',
//         website: 'http://example.com',
//         region: 'Europe',
//     },
// ];

// function loadPlaces(map, lat = 40.854700, lng = -73.877500) {

//     const bounds = new google.maps.LatLngBounds();
//     const infoWindow = new google.maps.InfoWindow();

//     const markers = places.map(place => {
//         const position = { lat: place.lat, lng: place.lng };
//         var icon;
        
//         // get different icon depending on type
//         if (place.type === 'silver') {
//             icon = 'images/marker-silver.png';
//         } else if (place.type === 'bronze') {
//             icon = 'images/marker-bronze.png';
//         } else if (place.type === 'gold') {
//             icon = 'images/marker-gold.png';
//         }

//         bounds.extend(position);
//         const marker = new google.maps.Marker({ map, position, icon });
//         marker.place = place;
//         return marker;
//     });

//     // when someone clicks on a marker, show the details of that place
//     markers.forEach(marker => marker.addListener('click', function() {
//         const html = `
//             <div class="popup">
//                 <div class="popup-head">
//                     <h5>${this.place.title}<span class="${this.place.type}">${this.place.subtitle}</span></h5>
//                     <ul class="pfl-info-list">
//                         <li><span>Regions Served:</span>${this.place.region}</li>
//                     </ul>
//                 </div>
//                 <div class="popup-body">
//                     <ul class="pfl-info-list">
//                         <li><span>Name:</span>${this.place.name}</li>
//                         <li><span>Title:</span>${this.place.position}</li>
//                         <li><span>Phone:</span>${this.place.phone}</li>
//                         <li><span>Email:</span>${this.place.email}</li>
//                         <li><span>Website:</span>${this.place.website}</li>
//                     </ul>
//                 </div>
//             </div>
//         `;
//         infoWindow.setContent(html);
//         infoWindow.open(map, this);
//     }));

//     // then zoom the map to fit all the markers perfectly
//     map.setCenter(bounds.getCenter());
//     map.fitBounds(bounds);

// }

// function makeMap(mapDiv) {
//     if (!mapDiv) return;

//     const map = new google.maps.Map(mapDiv, mapOptions);
//     loadPlaces(map);
// }

// makeMap(mapDiv);




// // MAP SINGLE - Training Page
// const mapSingle = document.getElementById('map-single');

// if (mapSingle) {
//     const mapData = {
//         lat: parseFloat(mapSingle.dataset.lat),
//         lng: parseFloat(mapSingle.dataset.lng),
//         title: mapSingle.dataset.title,
//         addr: mapSingle.dataset.addr
//     };
    
//     const map = new google.maps.Map(mapSingle, {
//         center: { lat: mapData.lat, lng: mapData.lng },
//         zoom: 15,
//         disableDefaultUI: true,
//     });

//     var infowindow = new google.maps.InfoWindow({
//         content: `
//             <div class="popup popup-addr">
//                 <h6>${mapData.title}</h6>
//                 <address>${mapData.addr}</address>
//             </div>
//         `
//     });

//     const marker = new google.maps.Marker({
//         map: map,
//         position: { lat: mapData.lat, lng: mapData.lng },
//         icon: 'images/marker-gold.png',
//     });

//     infowindow.open(mapSingle, marker);
// }









/* WP LIVE SITE MAP JS */
// API KEY: AIzaSyAd1_9wafTrKjPVPCH6t6MlhXrBJnDeFyQ

const mapDiv = document.getElementById('map');

const mapOptions = {
	center           : { lat: 40.8547, lng: -73.8775 },
	zoom             : 4,
	disableDefaultUI : true
};

function loadPlaces(map, lat = 40.8547, lng = -73.8775) {
	const bounds = new google.maps.LatLngBounds();
	const infoWindow = new google.maps.InfoWindow();

	$.ajax({
		type    : 'POST',
		url     : theme_ajax.ajax_url,
		data    : {
			action : 'partnersPlace'
		},
		success : function(data) {
			const markers = data.map((place) => {
				const position = { lat: parseFloat(place.lat), lng: parseFloat(place.lng) };
				var icon;

				// get different icon depending on type
				if (place.type === 'silver') {
					icon = theme_ajax.theme_url + '/images/marker-silver.png';
				} else if (place.type === 'bronze') {
					icon = theme_ajax.theme_url + '/images/marker-bronze.png';
				} else if (place.type === 'gold') {
					icon = theme_ajax.theme_url + '/images/marker-gold.png';
				}

				bounds.extend(position);
				const marker = new google.maps.Marker({ map, position, icon });
				marker.place = place;
				return marker;
			});
			// when someone clicks on a marker, show the details of that place
			markers.forEach((marker) =>
				marker.addListener('click', function() {
					const html = `
                <div class="popup">
                    <div class="popup-head">
                        <h5>${this.place.title}<span class="${this.place.type}">${this.place.subtitle}</span></h5>
                        <ul class="pfl-info-list">
                            <li><span>Regions Served:</span>${this.place.region}</li>
                        </ul>
                    </div>
                    <div class="popup-body">
                        <ul class="pfl-info-list">
                            <li><span>Name:</span>${this.place.name}</li>
                            <li><span>Title:</span>${this.place.position}</li>
                            <li><span>Phone:</span>${this.place.phone}</li>
                            <li><span>Email:</span>${this.place.email}</li>
                            <li><span>Website:</span>${this.place.website}</li>
                        </ul>
                    </div>
                </div>
            `;
					infoWindow.setContent(html);
					infoWindow.open(map, this);
				})
			);

			// then zoom the map to fit all the markers perfectly
			map.setCenter(bounds.getCenter());
			map.fitBounds(bounds);
		},
		error   : function(jqXHR, textStatus, errorThrown) {
			console.log(jqXHR + ' :: ' + textStatus + ' :: ' + errorThrown);
		}
	});
}

function makeMap(mapDiv) {
	if (!mapDiv) return;

	const map = new google.maps.Map(mapDiv, mapOptions);
	loadPlaces(map);
}

makeMap(mapDiv);

// MAP SINGLE - Training Page
const mapSingle = document.getElementById('map-single');

if (mapSingle) {
    const mapData = {
        lat: parseFloat(mapSingle.dataset.lat),
        lng: parseFloat(mapSingle.dataset.lng),
        title: mapSingle.dataset.title,
        addr: mapSingle.dataset.addr
    };
    
    const map = new google.maps.Map(mapSingle, {
        center: { lat: mapData.lat, lng: mapData.lng },
        zoom: 15,
        disableDefaultUI: true,
    });

    var infowindow = new google.maps.InfoWindow({
        content: `
            <div class="popup popup-addr">
                <h6>${mapData.title}</h6>
                <address>${mapData.addr}</address>
            </div>
        `
    });

    const marker = new google.maps.Marker({
        map: map,
        position: { lat: mapData.lat, lng: mapData.lng },
        icon: theme_ajax.theme_url + '/images/marker-gold.png',
    });

    infowindow.open(mapSingle, marker);
}