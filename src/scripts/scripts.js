(function($) {

    // document ready
    $(document).ready(function() {

        // change header style on scroll
        $(window).on('scroll', function() {
            var scrollTop = $(this).scrollTop();
            if (scrollTop >= 5) {
                $('.header').addClass('collapsed');
            } else {
                $('.header').removeClass('collapsed');
            }
        });

        // nav-toggler
        $('.nav-toggler').on('click', function(event) {
            event.preventDefault();
            $(this).next('.main-nav').slideToggle('fast');
        });

        // search
        var searchBox = $('.search-box');
        var searchClose = $('.search-box .btn-close');
        $('.search-link a').on('click', function(event) {
            event.preventDefault();
            searchBox.addClass('open');
            searchBox.find('.form-control').focus();
        });
        $(window).on('click', function(event) {
            event.stopPropagation();
            var target = $(event.target);
            if (target.is(searchBox) || target.is(searchClose)) {
                searchBox.removeClass('open');
            }
        });

        // magnific popup - video
        $('.video-popup-link').magnificPopup({
            type: 'iframe',
            iframe: {
                markup: '<div class="mfp-iframe-scaler">'+
                          '<div class="mfp-close"></div>'+
                          '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                        '</div>',
                patterns: {
                    youtube: {
                        index: 'youtube.com/',
                        id: 'v=',
                        src: 'https://www.youtube.com/embed/%id%?autoplay=1'
                    },
                    vimeo: {
                        index: 'vimeo.com/',
                        id: '/',
                        src: 'https://player.vimeo.com/video/%id%?autoplay=1'
                    },
                    gmaps: {
                        index: '//maps.google.',
                        src: '%id%&output=embed'
                    }
                },
                srcAction: 'iframe_src',
            }
        });

        // owl-carousel-logo
        $('.owl-carousel-logo').owlCarousel({
            nav: false,
            dots: false,
            // items: 7,
            autoHeight: true,
            loop: true,
            autoplay: true,
            autoplayTimeout: 4000,
            autoplaySpeed: 500,
            responsive: {
                0: {
                    items: 3
                },
                576: {
                    items: 4
                },
                768: {
                    items: 5
                },
                992: {
                    items: 6
                },
                1200: {
                    items: 7
                },
            }
        });

        // owl-carousel-industry
        $('.owl-carousel-industry').owlCarousel({
            nav: true,
            dots: false,
            items: 1,
            autoHeight: true,
            loop: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplaySpeed: 500,
            navText: ['<i class="fa fa-long-arrow-left"></i>', '<i class="fa fa-long-arrow-right"></i>'],
        });

        // owl-carousel-promo-vid
        $('.owl-carousel-promo-vid').owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplaySpeed: 500,
        });
        
        // owl-carousel-feedback
        $('.owl-carousel-feedback').owlCarousel({
            nav: true,
            dots: false,
            margin: 30,
            autoHeight: true,
            loop: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplaySpeed: 500,
            navText: ['<i class="fa fa-long-arrow-left"></i>', '<i class="fa fa-long-arrow-right"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                992: {
                    items: 2
                }
            }
        });

        // owl-carousel-msg
        $('.owl-carousel-msg').owlCarousel({
            nav: true,
            dots: false,
            autoHeight: true,
            items: 1,
            loop: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplaySpeed: 500,
            navText: ['<i class="fa fa-long-arrow-left"></i>', '<i class="fa fa-long-arrow-right"></i>'],
        });

        // owl-carousel-post
        $('.owl-carousel-post').owlCarousel({
            nav: true,
            dots: false,
            items: 3,
            margin: 30,
            loop: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplaySpeed: 500,
            navText: ['<i class="fa fa-long-arrow-left"></i>', '<i class="fa fa-long-arrow-right"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1200: {
                    items: 3
                }
            }
        });

        // table
        $('.pfl-info > a[class|="btn-circle"]').on('click', function(event) {
            event.preventDefault();
            $(this).closest('tbody').find('.pfl-box').hide();
            $(this).next('.pfl-box').show();
        });
        $('.pfl-box > a[class|="btn-circle"]').on('click', function(event) {
            event.preventDefault();
            $(this).parent('.pfl-box').hide();
        });

        // smooth scroll
        $(".btn-smooth-scroll").on('click', function(event) {
            if (this.hash !== "") {
                event.preventDefault();
                var hash = this.hash;
                $('html, body').animate({
                    scrollTop: $(hash).offset().top - 60
                }, 700);
            }
        });


        //////////////////////////////
        // js for wp side
        //////////////////////////////
        $( '#loadMore' ).on( "click", function ( event ) {
            event.preventDefault();

            $(this).text('Loading...');
            
            var offset = $( this ).attr( 'data-offset' );
            var nonce = $(this).attr('data-nonce');
            var str = {
                offset: offset,
                action: 'trainingLoadMore',
                security: nonce
            };
            $.ajax( {
                type: "POST",
                url: theme_ajax.ajax_url,
                data: str,
                success: function ( data ) {
                    var $html = $( data );
                    if ( $html.length ) {
                        $( '#trainingData' ).append( $html );
                        $( '#loadMore' ).attr( 'data-offset', parseInt( offset ) + 3 );
                    } else {
                        $( '#loadMore' ).closest( '.row' ).fadeOut();
                    }
                    $('#loadMore').text('Load More');
                },
                error: function ( jqXHR, textStatus, errorThrown ) {
                    $('#loadMore').text('Load More');
                    console.log( jqXHR + " :: " + textStatus + " :: " + errorThrown );
                }
            } );
            
            return false;
        } );

        
    });
    




    // window onload
    $(window).on('load', function() {

        // masonry
        $('.doc-box-wrap').masonry({
            itemSelector: '.doc-box'
        });

        // carousel fill arrow
        var industryMediaAll = $('.owl-carousel-industry .industry-media');
        industryMediaAll.each(function(industryMedia) {
            var industryMediaHeight = $(this).outerHeight() + 'px';
            $(this).children('span').css({borderBottomWidth: industryMediaHeight});
        });

    });

})(jQuery);