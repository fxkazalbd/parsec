// promo-dl
section.promo-dl
    .container
        .row
            .col-md-12
                .section-intro
                    h2 Harness The Power Of Data-Driven Decision Making
        .row
            .col-md-12
                .promo.promo-primary
                    .promo-media
                        img(src="images/promo-dl-media-01.png", alt="name")
                    .promo-intro
                        p Lack of performance can have a serious effect on your bottom line. Find out how the right software can help you run more effectively and efficiently.
                        form.form-dl(action="#")
                            .form-group
                                input.form-control(type="text" placeholder="Enter Your Email Address")
                            .form-group
                                button.btn.btn-blue.btn-block(type="submit") Download Now
                        p We're committed to your privacy. Parsec uses the information you provide to us to contact you about our relevant content, products, and services. You may unsubscribe from these communications at any time. For more information, check out our <a href="#">Privacy Policy.</a>

// promo-dl
section.promo-dl
    .container
        .row
            .col-md-12
                .section-intro
                    h2 Download The TrakSYS Brochure
                    p Get the TrakSYS brochure. All the information you need, in one convenient package.
        .row
            .col-md-12
                .promo.promo-secondary
                    .promo-form
                        form.form-dl(action="#")
                            .form-group
                                input.form-control(type="text" placeholder="Enter Your Email Address")
                            .form-group
                                button.btn.btn-blue.btn-block(type="submit") Download Brochure
                    .promo-intro
                        p We're committed to your privacy. Parsec uses the information you provide to us to contact you about our relevant content, products, and services. You may unsubscribe from these communications at any time. For more information, check out our <a href="#">Privacy Policy.</a>

// promo
.promo {
    display: flex;
    > div {
        flex: 0 0 auto;
        width: 100%;
    }
}
.promo-primary {
    justify-content: space-between;
    align-items: center;
    .promo-intro {
        max-width: 450px;
    }
    .form-dl {
        margin-top: 35px;
        margin-bottom: 15px;
    }
}
.promo-secondary {
    justify-content: center;
    align-items: flex-start;
    .promo-intro {
        max-width: 455px;
    }
}
.promo-media {
    max-width: 565px;
}
.promo-form {
    max-width: 450px;
}
.promo-intro {
    margin-left: 40px;
    p {
        font-size: 14px;
        line-height: 26px;
        a {
            display: inline-block;
            color: $blue;
            &:hover,
            &:active {
                text-decoration: underline;
            }
        }
    }
}
.form-dl {
    .form-group {
        margin-bottom: 20px;
        &:last-child {
            margin-bottom: 0;
        }
    }
    .form-control {
        height: 50px;
        background-color: $pale-white-light;
        border-radius: 25px;
        border: 0;
        padding: 5px 30px;
    }
}